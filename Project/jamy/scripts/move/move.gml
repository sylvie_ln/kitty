var amount = argument[0];
var axis = argument[1];
amount = clamp(amount,-12,12);
if amount == 0 { return true; }

remainder[axis] += amount;
var target = round_ties_down(remainder[axis]);
remainder[axis] -= target;

var xa = (axis == 0) ? 1 : 0;
var ya = (axis == 1) ? 1 : 0;
var s = sign(target);

while target != 0 {
	var collision = script_execute(check_collision,x+xa*s,y+ya*s);
	if collision[0] {
		var stop = script_execute(handle_collision,collision[1]);
		if stop {
			return false;
		}
	}
	if !place_free(x+xa*s,y+ya*s) {
		return false;	
	}
	x += xa*s;
	y += ya*s;
	target -= s;
}
return true;