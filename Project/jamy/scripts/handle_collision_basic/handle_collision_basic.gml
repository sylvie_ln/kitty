var list = argument[0];
var process = true;
if argument_count > 1 {
	process = argument[1];	
}
var stop = false;
for(var i=0; i<ds_list_size(list); i++) {
	var inst = list[|i];
	if inst.object_index == oBounds {
		stop = true;	
	}
	if inst.solid {
		stop = true;	
	}
	if object_is_ancestor(inst.object_index,oUpfloor) {
		if inst.bbox_top > bbox_bottom {
			if inst.object_index == oFlower and vv >= 0 {
				with inst {
					squish++;
					event_user(1);
				}
			} else if inst.object_index == oEgg and vv >= 0 {
				with inst {
					event_user(1);
				}
			} else {
				stop = true;
			}	
		}
	}
	if object_is_ancestor(inst.object_index,oItem) {
		with inst {
			event_user(0);	
		}
	}
}
ds_list_destroy(list);
return stop;