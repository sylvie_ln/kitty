var collision = script_execute(check_collision,x,y+1);
if collision[0] {
	return script_execute(handle_collision,collision[1],false);
}
return false;