var col = c_white;
var alpha = global.outline_alpha;
if argument_count >= 2 {
    alpha = argument[1];    
} 
if argument_count >= 1 {
    col = argument[0];
}
for(var i=-1;i<=1;i++) {
    for(var j=-1;j<=1;j++) {
        if intersect_rect(
        x+(room_width*i)-abs(sprite_xoffset),y+(room_height*j)-abs(sprite_yoffset),
        x+(room_width*i)-abs(sprite_xoffset)+abs(sprite_width)-1,
        y+(room_height*j)-abs(sprite_yoffset)+abs(sprite_height)-1,
        0,0,room_width-1,room_height-1) {
            draw_outline_ext(col,alpha,x+room_width*i,y+room_height*j);
        }
    }
}
