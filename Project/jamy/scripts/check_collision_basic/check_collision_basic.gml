var list = ds_list_create();
var num = instance_place_list(argument[0],argument[1],all,list,false);
if bbox_left+(argument[0]-x) < 0 
or bbox_right+(argument[0]-x) >= room_width 
or bbox_top+(argument[1]-y) < 0 
or bbox_bottom+(argument[1]-y) >= room_height-20 {
	ds_list_add(list,oBounds.id);
	num += 1;
}
return [num != 0,list];