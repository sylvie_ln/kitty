///@description Read input of a given type for the given action.
///@param action_name The name of the action to read input for
///@param input_read_type Type of input to read (held, pressed, released, etc.)
with oInput {
	var action = argument[0];
	var type = argument[1];
	var list = actions[? action];
	if type == input_read_type.held_time {
		var held_time = held_time_map[? action];
		return held_time;
	}
	if type == input_read_type.pressed_repeat {
		var held_time = held_time_map[? action];
		if argument_count > 3 {
			var repeat_speed = argument[2];
			var repeat_delay = argument[3];
		} else {
			var repeat_speed = global.key_repeat_speed;
			var repeat_delay = global.key_repeat_delay;
		}				
		return ((held_time >= repeat_delay) 
				and ((held_time-repeat_delay) mod repeat_speed) == 0)
				or input_pressed(argument[0]);
	}
	for(var i=0; i< ds_list_size(list); i++) {
		var entry = list[|i];
		var kind = entry[|0];
		if type == input_read_type.raw_axis and kind != input_kind.gamepad_axis {
			continue;	
		}
		switch(kind) {
			case input_kind.key:
				var key = entry[|1];
				switch(type) {
					case input_read_type.held:
						if keyboard_check(key) {
							return true;	
						}
					break;
					case input_read_type.pressed:
					case input_read_type.pressed_repeat:
						if keyboard_check_pressed(key) {
							return true;	
						}
					break;
					case input_read_type.released:
						if keyboard_check_released(key) {
							return true;	
						}
					break;
				}
			break;
			case input_kind.gamepad_button:
				var button = entry[|1];
				var gpad = -1;
				if multiplayer and ds_map_exists(gpad_map,gpid) {
					var gpad = gpad_map[? gpid];	
				}
				for(var g=0; g<gamepad_get_device_count(); g++) {
					if gpad != -1 and g != gpad { continue; }
					if !gamepad_is_connected(g) { continue; }
					switch(type) {
						case input_read_type.held:
							if gamepad_button_check(g,button) {
								return true;	
							}
						break;
						case input_read_type.pressed:
						case input_read_type.pressed_repeat:
							if gamepad_button_check_pressed(g,button) {
								return true;	
							}
						break;
						case input_read_type.released:
							if gamepad_button_check_released(g,button) {
								return true;	
							}
						break;
					}
				}
			break;
			case input_kind.gamepad_axis:
				var axis = entry[|1];
				var dir = entry[|2];
				var gpad = -1;
				if multiplayer and ds_map_exists(gpad_map,gpid) {
					var gpad = gpad_map[? gpid];	
				}
				for(var g=0; g<gamepad_get_device_count(); g++) {
					if gpad != -1 and g != gpad { continue; }
					if !gamepad_is_connected(g) { continue; }
					gamepad_set_axis_deadzone(g,0.5);
					if type == input_read_type.raw_axis {
						return gamepad_axis_value(g,axis);	
					}
					var asign = sign(gamepad_axis_value(g,axis));
					var prev = previous_axis_map[? string(g)+":"+string(axis)];
					if is_undefined(prev) {
						var psign = 0;	
					} else {
						var psign = sign(prev);
					}
					switch(type) {
						case input_read_type.held:
							if asign == dir {
								return true;	
							}
						break;
						case input_read_type.pressed:
						case input_read_type.pressed_repeat:
							if asign != psign and asign == dir {
								return true;	
							}
						break;
						case input_read_type.released:
							if psign == dir and asign == 0 {
								return true;	
							}
						break;
					}
				}
			break;
			case input_kind.gamepad_hat:
				var hat = entry[|1];
				var dir = entry[|2];
				var gpad = -1;
				if multiplayer and ds_map_exists(gpad_map,gpid) {
					var gpad = gpad_map[? gpid];	
				}
				for(var g=0; g<gamepad_get_device_count(); g++) {
					if gpad != -1 and g != gpad { continue; }
					if !gamepad_is_connected(g) { continue; }
					var hdir = gamepad_hat_value(g,hat);
					if is_undefined(hdir) { continue; }
					var pdir = previous_hat_map[? string(g)+":"+string(hat)];
					if is_undefined(pdir) { pdir = 0; }
					switch(type) {
						case input_read_type.held:
							if (hdir & dir) == dir {
								return true;	
							}
						break;
						case input_read_type.pressed:
						case input_read_type.pressed_repeat:
							if (hdir & dir) == dir and (pdir & dir) != dir {
								return true;	
							}
						break;
						case input_read_type.released:
							if (hdir & dir) != dir  and (pdir & dir) == dir {
								return true;	
							}
						break;
					}
				}
			break;
		}
	}
	return false;
}