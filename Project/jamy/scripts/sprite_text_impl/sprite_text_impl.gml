var xx = argument[0];
var yy = argument[1];
var text = argument[2];
var mode = argument[3];
var width = 0;

enum sprite_text_modes {
	draw,
	width
}

var text_buffer = "";
for(var i=1; i<=string_length(text); i++) {
	var c = string_char_at(text,i);
	if c != "\\" {
		text_buffer += c;
	} else {
		if i == string_length(text) {
			text_buffer += "\\";
		} else if string_char_at(text,i+1) == "\\" {
			text_buffer += "\\";
			i++;
		} else {
			switch(mode) {
				case sprite_text_modes.draw:
					draw_text(xx,yy,text_buffer);
				break;
				case sprite_text_modes.width:
					width += string_width(text_buffer);
				break;
			}
			xx += string_width(text_buffer);
			text_buffer = "";
			
			var sprite = "";
			var j = i+1;
			while true {
				var d = string_char_at(text,j);
				if d == ":" {
					break;	
				}
				sprite += d;
				j++;
			}
			sprite = asset_get_index(sprite);
			
			j++;
			var sprite_image = "";
			while true {
				if j > string_length(text) {
					break;	
				}
				var d = string_char_at(text,j);
				if ord(d) < ord("0") or ord(d) > ord("9") {
					break;	
				}	
				sprite_image += d;
				j++;
			}
			sprite_image = real(sprite_image);
			
			switch(mode) {
				case sprite_text_modes.draw:
					draw_sprite(sprite,sprite_image,xx+sprite_get_xoffset(sprite),yy+sprite_get_yoffset(sprite));
				break;
				case sprite_text_modes.width:
					width += sprite_get_width(sprite);
				break;
			}
			xx += sprite_get_width(sprite);
			
			i = j-1;
		}
	}
}
if text_buffer != "" {
	switch(mode) {
		case sprite_text_modes.draw:
			draw_text(xx,yy,text_buffer);
		break;
		case sprite_text_modes.width:
			width += string_width(text_buffer);
		break;
	}
}
if mode == sprite_text_modes.width {
	return width;	
}