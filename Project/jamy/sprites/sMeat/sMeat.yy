{
    "id": "54969dd3-91df-42dd-b722-903fe1959341",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMeat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "65f146c8-1a3a-4496-bb21-d0bfdd63f478",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54969dd3-91df-42dd-b722-903fe1959341",
            "compositeImage": {
                "id": "feac8ae6-9ad7-42f5-8492-4390681e4d33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65f146c8-1a3a-4496-bb21-d0bfdd63f478",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e00a939-b8e0-4dbb-9f60-fd8e529fa8d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65f146c8-1a3a-4496-bb21-d0bfdd63f478",
                    "LayerId": "01bb2de9-0d95-4be5-86fc-f62fee9f5903"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "01bb2de9-0d95-4be5-86fc-f62fee9f5903",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "54969dd3-91df-42dd-b722-903fe1959341",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}