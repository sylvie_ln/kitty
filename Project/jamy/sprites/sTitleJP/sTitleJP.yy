{
    "id": "528392f1-8903-4520-bdd7-24031e77678d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTitleJP",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 179,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5baac7d4-be76-4bd2-8648-9306ae6825a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "528392f1-8903-4520-bdd7-24031e77678d",
            "compositeImage": {
                "id": "bfb918a0-1678-495d-b5ce-839539264eee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5baac7d4-be76-4bd2-8648-9306ae6825a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41dc9f6e-35ae-4c38-95e9-998df5261bdb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5baac7d4-be76-4bd2-8648-9306ae6825a6",
                    "LayerId": "3be54cda-3d2d-4577-a985-b49c155c8135"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "3be54cda-3d2d-4577-a985-b49c155c8135",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "528392f1-8903-4520-bdd7-24031e77678d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 160,
    "yorig": 90
}