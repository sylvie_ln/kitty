{
    "id": "dcf78673-d600-44ec-877a-12e504194c10",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGroundPink",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4908b82d-eee1-4e80-a686-487e3f37aab7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dcf78673-d600-44ec-877a-12e504194c10",
            "compositeImage": {
                "id": "3dfd0405-46dc-41c6-b8c0-c69b0d8eefeb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4908b82d-eee1-4e80-a686-487e3f37aab7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11ef1e3e-c2c6-44a8-b79e-5702befbd7fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4908b82d-eee1-4e80-a686-487e3f37aab7",
                    "LayerId": "00fae083-32b1-4fe0-9d36-6e45c4b0b385"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "00fae083-32b1-4fe0-9d36-6e45c4b0b385",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dcf78673-d600-44ec-877a-12e504194c10",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 160,
    "yorig": 80
}