{
    "id": "762a4fe0-7c12-4c28-8b7f-2dc4f1d00d0a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBackground3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 179,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c593419b-c5d8-4429-b275-e95606615462",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "762a4fe0-7c12-4c28-8b7f-2dc4f1d00d0a",
            "compositeImage": {
                "id": "3643ba18-9c21-4083-afdb-b99eb588a040",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c593419b-c5d8-4429-b275-e95606615462",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84555829-ca1b-4908-9741-60e2daaa4377",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c593419b-c5d8-4429-b275-e95606615462",
                    "LayerId": "62a90c4d-8e4d-4ec8-879b-13bb966326ba"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "62a90c4d-8e4d-4ec8-879b-13bb966326ba",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "762a4fe0-7c12-4c28-8b7f-2dc4f1d00d0a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 160,
    "yorig": 90
}