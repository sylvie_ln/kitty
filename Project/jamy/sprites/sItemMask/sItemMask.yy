{
    "id": "47656266-f128-4417-adc1-7d34c17390f0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sItemMask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 10,
    "bbox_right": 21,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f06e3b69-a713-4ff9-8fb4-3025438166b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47656266-f128-4417-adc1-7d34c17390f0",
            "compositeImage": {
                "id": "81ab55bd-b432-4250-be34-0828ec1cdf66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f06e3b69-a713-4ff9-8fb4-3025438166b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a316d261-3503-406f-899f-cf101bb3258f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f06e3b69-a713-4ff9-8fb4-3025438166b4",
                    "LayerId": "0b2f128e-4422-43d6-8bd9-33b8faf1823c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0b2f128e-4422-43d6-8bd9-33b8faf1823c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "47656266-f128-4417-adc1-7d34c17390f0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}