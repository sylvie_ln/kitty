{
    "id": "ccbcd56d-e4b9-4235-87f4-c7e1c23ba859",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sB32",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2b1d8ccf-bdd2-4276-8dd3-adddffa47ec3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccbcd56d-e4b9-4235-87f4-c7e1c23ba859",
            "compositeImage": {
                "id": "1180888a-0c95-45bb-bd97-eaa684c99804",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b1d8ccf-bdd2-4276-8dd3-adddffa47ec3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ade32a1-7382-430c-ac89-124146ba5830",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b1d8ccf-bdd2-4276-8dd3-adddffa47ec3",
                    "LayerId": "ca39e48a-5c15-4ea2-b96c-e13bb3b35df3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ca39e48a-5c15-4ea2-b96c-e13bb3b35df3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ccbcd56d-e4b9-4235-87f4-c7e1c23ba859",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}