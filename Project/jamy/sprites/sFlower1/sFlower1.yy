{
    "id": "16e27232-24b3-4706-b673-a92688ca884a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFlower1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 4,
    "bbox_right": 28,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6ada066b-577b-4ada-b96e-ed0ede995221",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16e27232-24b3-4706-b673-a92688ca884a",
            "compositeImage": {
                "id": "eff7e80c-ddd2-416f-a8de-83a1dc72c175",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ada066b-577b-4ada-b96e-ed0ede995221",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd7d6790-6601-4f29-b337-f8a86fe9637d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ada066b-577b-4ada-b96e-ed0ede995221",
                    "LayerId": "591b1a2f-6627-4789-8655-29ca5f2a157e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "591b1a2f-6627-4789-8655-29ca5f2a157e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "16e27232-24b3-4706-b673-a92688ca884a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}