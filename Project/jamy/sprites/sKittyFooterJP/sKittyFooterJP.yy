{
    "id": "9f52894d-fc27-41f8-b36c-70142d4460f3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sKittyFooterJP",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "89deb952-798e-4c4e-80a4-4d4f91bffca8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f52894d-fc27-41f8-b36c-70142d4460f3",
            "compositeImage": {
                "id": "9d286f4c-5f26-4244-891e-e50b97770121",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89deb952-798e-4c4e-80a4-4d4f91bffca8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a0be4d2-cf28-485d-bfe6-cb1882039494",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89deb952-798e-4c4e-80a4-4d4f91bffca8",
                    "LayerId": "d63a9335-f76b-4b41-b02e-6ed0ca6fa93a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "d63a9335-f76b-4b41-b02e-6ed0ca6fa93a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9f52894d-fc27-41f8-b36c-70142d4460f3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 0,
    "yorig": 0
}