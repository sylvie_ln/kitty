{
    "id": "3afb510f-ea2e-438d-803c-35a83247dfdd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFlower5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 5,
    "bbox_right": 28,
    "bbox_top": 22,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3c679d17-d147-4923-9e10-601926f8a4cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3afb510f-ea2e-438d-803c-35a83247dfdd",
            "compositeImage": {
                "id": "64f14d22-73fe-4806-ac33-33e4cdfb2101",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c679d17-d147-4923-9e10-601926f8a4cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6c9cb05-bace-498c-99bf-70afb39b5105",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c679d17-d147-4923-9e10-601926f8a4cd",
                    "LayerId": "285039c1-3998-4a94-be5e-6be15a59e593"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "285039c1-3998-4a94-be5e-6be15a59e593",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3afb510f-ea2e-438d-803c-35a83247dfdd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}