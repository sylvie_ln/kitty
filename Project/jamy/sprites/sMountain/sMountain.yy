{
    "id": "b185d082-fa27-48ed-bcbb-9f2999c00db2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMountain",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "02b8c581-71a2-4aa0-856d-2adecb98fd2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b185d082-fa27-48ed-bcbb-9f2999c00db2",
            "compositeImage": {
                "id": "74f4c38e-5fdc-4037-8f4c-6abb5d2739a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02b8c581-71a2-4aa0-856d-2adecb98fd2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fb47beb-acf1-4ca7-88e9-1394ec527ba1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02b8c581-71a2-4aa0-856d-2adecb98fd2a",
                    "LayerId": "84877211-27a9-49ac-bad3-d0861ebbde50"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "84877211-27a9-49ac-bad3-d0861ebbde50",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b185d082-fa27-48ed-bcbb-9f2999c00db2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}