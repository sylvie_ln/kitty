{
    "id": "4ebe0adc-d323-4261-acfb-c94c655801eb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sKittey",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "abad41f7-9f87-4e5f-9523-838428f5609b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ebe0adc-d323-4261-acfb-c94c655801eb",
            "compositeImage": {
                "id": "c89efa1f-5c3d-443a-ad41-6e3c7250b617",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abad41f7-9f87-4e5f-9523-838428f5609b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82094055-e79c-49c3-a86b-5d86722a4133",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abad41f7-9f87-4e5f-9523-838428f5609b",
                    "LayerId": "7c540eb9-48d3-4738-af7a-258987c76bcf"
                }
            ]
        },
        {
            "id": "6f668459-b09e-4b56-b3ea-7846a09f6159",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ebe0adc-d323-4261-acfb-c94c655801eb",
            "compositeImage": {
                "id": "e31db130-af44-41bd-ad66-9eaf0d8ed681",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f668459-b09e-4b56-b3ea-7846a09f6159",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cbd2641-7bff-4fc5-ab52-bf11bf32e0cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f668459-b09e-4b56-b3ea-7846a09f6159",
                    "LayerId": "7c540eb9-48d3-4738-af7a-258987c76bcf"
                }
            ]
        },
        {
            "id": "94747426-81f6-4dde-a521-e64616119607",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ebe0adc-d323-4261-acfb-c94c655801eb",
            "compositeImage": {
                "id": "8a26b6ff-e5e6-42c3-9a5c-65455ee173ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94747426-81f6-4dde-a521-e64616119607",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9568216-13d4-48fa-b5da-0d3444712577",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94747426-81f6-4dde-a521-e64616119607",
                    "LayerId": "7c540eb9-48d3-4738-af7a-258987c76bcf"
                }
            ]
        },
        {
            "id": "d8f87605-ddee-485b-bdec-62c1f1100c36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ebe0adc-d323-4261-acfb-c94c655801eb",
            "compositeImage": {
                "id": "46926702-6444-497c-a5d4-1ddf066070b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8f87605-ddee-485b-bdec-62c1f1100c36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63a90403-94c0-4b0e-ab5d-0ab11071afc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8f87605-ddee-485b-bdec-62c1f1100c36",
                    "LayerId": "7c540eb9-48d3-4738-af7a-258987c76bcf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7c540eb9-48d3-4738-af7a-258987c76bcf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4ebe0adc-d323-4261-acfb-c94c655801eb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}