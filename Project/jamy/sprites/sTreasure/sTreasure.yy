{
    "id": "d4a244b6-f95d-4ff8-8998-365be4898afd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTreasure",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "91e16392-190c-4d0d-abe1-7a30dbe1391b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4a244b6-f95d-4ff8-8998-365be4898afd",
            "compositeImage": {
                "id": "23ab6da1-268b-481b-8cfd-bffdd8f05b4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91e16392-190c-4d0d-abe1-7a30dbe1391b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e2c4a11-8873-489a-bdc7-5a4c124ff89d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91e16392-190c-4d0d-abe1-7a30dbe1391b",
                    "LayerId": "beaa53a8-0896-41e1-81c2-16827193b550"
                }
            ]
        },
        {
            "id": "9e2f4852-e9c2-4f37-8fc1-1b5d27330f03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4a244b6-f95d-4ff8-8998-365be4898afd",
            "compositeImage": {
                "id": "e43e233b-04cf-4a9b-92fa-8d5db6392f3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e2f4852-e9c2-4f37-8fc1-1b5d27330f03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d4a3c31-179c-4e01-b331-f8f4c55ede50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e2f4852-e9c2-4f37-8fc1-1b5d27330f03",
                    "LayerId": "beaa53a8-0896-41e1-81c2-16827193b550"
                }
            ]
        },
        {
            "id": "4bc5742d-f22b-490f-aea0-7e89a8484a2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4a244b6-f95d-4ff8-8998-365be4898afd",
            "compositeImage": {
                "id": "e57819a4-1ecd-482f-aefe-866069beddc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bc5742d-f22b-490f-aea0-7e89a8484a2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "396d87b2-b9a6-46bd-afed-55b0b1c6230b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bc5742d-f22b-490f-aea0-7e89a8484a2e",
                    "LayerId": "beaa53a8-0896-41e1-81c2-16827193b550"
                }
            ]
        },
        {
            "id": "473db963-7880-446f-bf14-9f8c04b5a26a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4a244b6-f95d-4ff8-8998-365be4898afd",
            "compositeImage": {
                "id": "caf20838-9975-4e82-9b22-efc42f7bc2a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "473db963-7880-446f-bf14-9f8c04b5a26a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ecdc2ef-64b3-49d3-9f0c-6434dd7c8565",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "473db963-7880-446f-bf14-9f8c04b5a26a",
                    "LayerId": "beaa53a8-0896-41e1-81c2-16827193b550"
                }
            ]
        },
        {
            "id": "108faf40-db3b-4f03-a14b-83e7569fd284",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4a244b6-f95d-4ff8-8998-365be4898afd",
            "compositeImage": {
                "id": "b7a8493b-cd2a-44d4-907b-6cc56b47632c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "108faf40-db3b-4f03-a14b-83e7569fd284",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14d48c16-96ee-47d2-8334-c905381c5aec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "108faf40-db3b-4f03-a14b-83e7569fd284",
                    "LayerId": "beaa53a8-0896-41e1-81c2-16827193b550"
                }
            ]
        },
        {
            "id": "2a3b8731-1c0e-42e2-b746-2859c71c4830",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4a244b6-f95d-4ff8-8998-365be4898afd",
            "compositeImage": {
                "id": "1931f0f2-181f-4e54-86d0-5e7eebac825b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a3b8731-1c0e-42e2-b746-2859c71c4830",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "715bdf4a-4ced-43b3-8573-7b55280377cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a3b8731-1c0e-42e2-b746-2859c71c4830",
                    "LayerId": "beaa53a8-0896-41e1-81c2-16827193b550"
                }
            ]
        },
        {
            "id": "56148f27-75de-4853-b626-0549de19282e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4a244b6-f95d-4ff8-8998-365be4898afd",
            "compositeImage": {
                "id": "2d545be9-54b3-4699-881b-0f5e894cd429",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56148f27-75de-4853-b626-0549de19282e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98beb938-9081-41d2-88a8-11cc8042121e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56148f27-75de-4853-b626-0549de19282e",
                    "LayerId": "beaa53a8-0896-41e1-81c2-16827193b550"
                }
            ]
        },
        {
            "id": "bffe6b5e-6d18-45cb-9614-75fafe3368dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4a244b6-f95d-4ff8-8998-365be4898afd",
            "compositeImage": {
                "id": "987be838-13bc-476f-853d-f8a52290fc9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bffe6b5e-6d18-45cb-9614-75fafe3368dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa51d4cd-c6f8-480e-8a14-58f43b4e8593",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bffe6b5e-6d18-45cb-9614-75fafe3368dc",
                    "LayerId": "beaa53a8-0896-41e1-81c2-16827193b550"
                }
            ]
        },
        {
            "id": "82277604-4b56-49c7-ba35-34fdde444e67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4a244b6-f95d-4ff8-8998-365be4898afd",
            "compositeImage": {
                "id": "a86a68fa-3cdb-4c76-81a4-27a65bd63c9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82277604-4b56-49c7-ba35-34fdde444e67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ece0ef3-4979-4a36-bc07-e4929ab53e25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82277604-4b56-49c7-ba35-34fdde444e67",
                    "LayerId": "beaa53a8-0896-41e1-81c2-16827193b550"
                }
            ]
        },
        {
            "id": "c1d69240-a4b5-4409-9ebd-4a1abaea4515",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4a244b6-f95d-4ff8-8998-365be4898afd",
            "compositeImage": {
                "id": "6f4bc567-cbdb-46d3-a415-7097555da16c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1d69240-a4b5-4409-9ebd-4a1abaea4515",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f5e848c-6277-4d49-8ba4-b3d659def8dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1d69240-a4b5-4409-9ebd-4a1abaea4515",
                    "LayerId": "beaa53a8-0896-41e1-81c2-16827193b550"
                }
            ]
        },
        {
            "id": "bbedd683-f017-4005-b80d-ba6938f419ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4a244b6-f95d-4ff8-8998-365be4898afd",
            "compositeImage": {
                "id": "86732a75-dc89-4187-b43f-5d809ed27483",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbedd683-f017-4005-b80d-ba6938f419ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fe36a80-29f8-405d-8ba5-f2e945d7a19b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbedd683-f017-4005-b80d-ba6938f419ee",
                    "LayerId": "beaa53a8-0896-41e1-81c2-16827193b550"
                }
            ]
        },
        {
            "id": "0a94ec50-8fcd-419b-ac65-de507a3646f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4a244b6-f95d-4ff8-8998-365be4898afd",
            "compositeImage": {
                "id": "aa94864b-8cb9-4bf4-8b5e-5564f807f9d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a94ec50-8fcd-419b-ac65-de507a3646f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1173097a-44ff-41a7-a5df-f3c4c7c8e6d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a94ec50-8fcd-419b-ac65-de507a3646f0",
                    "LayerId": "beaa53a8-0896-41e1-81c2-16827193b550"
                }
            ]
        },
        {
            "id": "f2625683-9d53-41b5-bedf-b855c1d2394e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4a244b6-f95d-4ff8-8998-365be4898afd",
            "compositeImage": {
                "id": "e5c26b22-cfc8-4b2b-a618-3ba3329348a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2625683-9d53-41b5-bedf-b855c1d2394e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3e2365f-cbb1-495f-ad37-2120edd4c79e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2625683-9d53-41b5-bedf-b855c1d2394e",
                    "LayerId": "beaa53a8-0896-41e1-81c2-16827193b550"
                }
            ]
        },
        {
            "id": "8694dc8b-8dec-425f-90c8-c73482a44de8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4a244b6-f95d-4ff8-8998-365be4898afd",
            "compositeImage": {
                "id": "3e116c92-45bb-4818-b5ef-9bc4301f1f0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8694dc8b-8dec-425f-90c8-c73482a44de8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0b31d85-5828-4f3d-bd68-de1a0dea4017",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8694dc8b-8dec-425f-90c8-c73482a44de8",
                    "LayerId": "beaa53a8-0896-41e1-81c2-16827193b550"
                }
            ]
        },
        {
            "id": "8c67f131-e239-4bf9-89bc-28b07e29bd84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4a244b6-f95d-4ff8-8998-365be4898afd",
            "compositeImage": {
                "id": "322ca64d-cb67-4a49-b232-70d7e122dede",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c67f131-e239-4bf9-89bc-28b07e29bd84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb610c63-0b0d-42f0-bb28-7741bb855881",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c67f131-e239-4bf9-89bc-28b07e29bd84",
                    "LayerId": "beaa53a8-0896-41e1-81c2-16827193b550"
                }
            ]
        },
        {
            "id": "086a92e6-6943-4c66-9b9a-da3ff18f699c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4a244b6-f95d-4ff8-8998-365be4898afd",
            "compositeImage": {
                "id": "e86e954c-5d5c-495a-9716-b173f1983be7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "086a92e6-6943-4c66-9b9a-da3ff18f699c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa2b2448-15e6-49f1-8382-a19ecc4fa425",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "086a92e6-6943-4c66-9b9a-da3ff18f699c",
                    "LayerId": "beaa53a8-0896-41e1-81c2-16827193b550"
                }
            ]
        },
        {
            "id": "d7a789b6-123f-4452-bdc6-927d5746cb19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4a244b6-f95d-4ff8-8998-365be4898afd",
            "compositeImage": {
                "id": "3c5748ef-4480-4b59-9415-5f6d78cb33e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7a789b6-123f-4452-bdc6-927d5746cb19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6b8d537-e2d1-4e64-ac92-5bb5f5f769cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7a789b6-123f-4452-bdc6-927d5746cb19",
                    "LayerId": "beaa53a8-0896-41e1-81c2-16827193b550"
                }
            ]
        },
        {
            "id": "66f11c16-d6bd-4030-bfef-eb4cdf49f7a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4a244b6-f95d-4ff8-8998-365be4898afd",
            "compositeImage": {
                "id": "3e6342e5-56ad-46d7-9e2d-1d9c7327c742",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66f11c16-d6bd-4030-bfef-eb4cdf49f7a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41785fad-6dde-4405-9fef-fd4aebf5cfff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66f11c16-d6bd-4030-bfef-eb4cdf49f7a9",
                    "LayerId": "beaa53a8-0896-41e1-81c2-16827193b550"
                }
            ]
        },
        {
            "id": "08c95aa2-434d-4f89-88bf-2be7e7ff515b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4a244b6-f95d-4ff8-8998-365be4898afd",
            "compositeImage": {
                "id": "981d87f1-2dca-4b62-803e-604075c034ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08c95aa2-434d-4f89-88bf-2be7e7ff515b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33e1ea28-3a95-4a6c-b5ba-92886c14a164",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08c95aa2-434d-4f89-88bf-2be7e7ff515b",
                    "LayerId": "beaa53a8-0896-41e1-81c2-16827193b550"
                }
            ]
        },
        {
            "id": "2223ee3f-cc0f-42d7-92a7-7cdf0cb20ae3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4a244b6-f95d-4ff8-8998-365be4898afd",
            "compositeImage": {
                "id": "dfa70ea5-8b39-4875-95bd-30055347c0bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2223ee3f-cc0f-42d7-92a7-7cdf0cb20ae3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e24f4b9e-fb8b-4daa-bba8-3b9afe930470",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2223ee3f-cc0f-42d7-92a7-7cdf0cb20ae3",
                    "LayerId": "beaa53a8-0896-41e1-81c2-16827193b550"
                }
            ]
        },
        {
            "id": "3f9a1db5-0408-45a1-affd-c7c3fb23a362",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4a244b6-f95d-4ff8-8998-365be4898afd",
            "compositeImage": {
                "id": "3b3e9945-97e5-4aeb-b50c-a33480baff8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f9a1db5-0408-45a1-affd-c7c3fb23a362",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13b650da-60f3-44c8-a535-f8e47a85541d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f9a1db5-0408-45a1-affd-c7c3fb23a362",
                    "LayerId": "beaa53a8-0896-41e1-81c2-16827193b550"
                }
            ]
        },
        {
            "id": "4bfa14d4-9d0c-40d8-bea8-0d6dd0938c25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4a244b6-f95d-4ff8-8998-365be4898afd",
            "compositeImage": {
                "id": "96aed763-619d-4096-b934-c801befa36d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bfa14d4-9d0c-40d8-bea8-0d6dd0938c25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e7f2692-e057-47c3-b757-7a227259822a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bfa14d4-9d0c-40d8-bea8-0d6dd0938c25",
                    "LayerId": "beaa53a8-0896-41e1-81c2-16827193b550"
                }
            ]
        },
        {
            "id": "8319a515-6b26-42ca-a219-45d50d1678d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4a244b6-f95d-4ff8-8998-365be4898afd",
            "compositeImage": {
                "id": "215ea3dd-f60e-451c-9f2e-0ec704a6b18d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8319a515-6b26-42ca-a219-45d50d1678d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a469f721-c467-48b4-898d-1b2c40628fa8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8319a515-6b26-42ca-a219-45d50d1678d5",
                    "LayerId": "beaa53a8-0896-41e1-81c2-16827193b550"
                }
            ]
        },
        {
            "id": "a5807bbb-ef95-496b-8a49-41c699a11805",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4a244b6-f95d-4ff8-8998-365be4898afd",
            "compositeImage": {
                "id": "ef1079fc-4cf6-48e8-9a64-f58777a228db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5807bbb-ef95-496b-8a49-41c699a11805",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eeb0e773-d497-4d15-b711-19c2c15807cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5807bbb-ef95-496b-8a49-41c699a11805",
                    "LayerId": "beaa53a8-0896-41e1-81c2-16827193b550"
                }
            ]
        },
        {
            "id": "48364aa8-821d-4de0-8af2-bd7bad72e348",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4a244b6-f95d-4ff8-8998-365be4898afd",
            "compositeImage": {
                "id": "1292a65c-2a68-48e8-bec1-8e2848a8ec45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48364aa8-821d-4de0-8af2-bd7bad72e348",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff920e67-05a6-451d-80f4-01afd843766d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48364aa8-821d-4de0-8af2-bd7bad72e348",
                    "LayerId": "beaa53a8-0896-41e1-81c2-16827193b550"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "beaa53a8-0896-41e1-81c2-16827193b550",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d4a244b6-f95d-4ff8-8998-365be4898afd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}