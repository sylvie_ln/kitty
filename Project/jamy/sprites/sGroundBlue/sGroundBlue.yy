{
    "id": "796078b8-3b29-4e29-a2a2-d44107b9f935",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGroundBlue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9f60044e-b8cd-4ab2-bf34-169a6d531dd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "796078b8-3b29-4e29-a2a2-d44107b9f935",
            "compositeImage": {
                "id": "2611bfda-627e-4423-9146-64a6b2a9725d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f60044e-b8cd-4ab2-bf34-169a6d531dd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f11443ca-58c8-4ed2-a4f8-9cde92a57a23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f60044e-b8cd-4ab2-bf34-169a6d531dd8",
                    "LayerId": "846f49ac-b828-4fd3-b0ca-109e684ffa94"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "846f49ac-b828-4fd3-b0ca-109e684ffa94",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "796078b8-3b29-4e29-a2a2-d44107b9f935",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 160,
    "yorig": 80
}