{
    "id": "d0ccc443-352d-467d-ae42-116c3d88a1f1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBackground1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 179,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6dfb05f9-b821-4831-a926-864294398a50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0ccc443-352d-467d-ae42-116c3d88a1f1",
            "compositeImage": {
                "id": "053d3314-c77f-4f4e-9399-899b626105c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6dfb05f9-b821-4831-a926-864294398a50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8d10d65-3ed5-40ca-a11b-0956efec9d90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6dfb05f9-b821-4831-a926-864294398a50",
                    "LayerId": "4220868e-4708-4855-bec2-2bca76ab8939"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "4220868e-4708-4855-bec2-2bca76ab8939",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d0ccc443-352d-467d-ae42-116c3d88a1f1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 160,
    "yorig": 90
}