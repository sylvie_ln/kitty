{
    "id": "c8fadaaa-1f53-4a12-b03e-42dd24bc4161",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sKittyFooter",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "01a12fed-bbd0-4de8-a899-475990064057",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8fadaaa-1f53-4a12-b03e-42dd24bc4161",
            "compositeImage": {
                "id": "a7e3cf90-9972-4809-a795-4dadc6e4ee07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01a12fed-bbd0-4de8-a899-475990064057",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3098a95-e93a-4595-ac2d-b7b93c6fe1ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01a12fed-bbd0-4de8-a899-475990064057",
                    "LayerId": "40c4b682-4787-4a19-8c86-3957d1bca81b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "40c4b682-4787-4a19-8c86-3957d1bca81b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c8fadaaa-1f53-4a12-b03e-42dd24bc4161",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 0,
    "yorig": 0
}