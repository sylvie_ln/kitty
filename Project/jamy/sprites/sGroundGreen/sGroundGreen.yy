{
    "id": "2cd1483a-9cae-4245-8a36-526b6e4d78d1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGroundGreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "671d334f-3181-4da7-b936-d22e8ea14f8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2cd1483a-9cae-4245-8a36-526b6e4d78d1",
            "compositeImage": {
                "id": "727415b9-da32-467f-a1a2-e23119e93312",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "671d334f-3181-4da7-b936-d22e8ea14f8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9c2e0d8-7e1f-4980-bbff-a42ae6559c93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "671d334f-3181-4da7-b936-d22e8ea14f8d",
                    "LayerId": "afba0e37-c632-4048-9552-2d0ed24d27fa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "afba0e37-c632-4048-9552-2d0ed24d27fa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2cd1483a-9cae-4245-8a36-526b6e4d78d1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 160,
    "yorig": 80
}