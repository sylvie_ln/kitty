{
    "id": "56f52c63-7b3f-404a-a8d0-fb0fb3c8eb43",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSword",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 5,
    "bbox_right": 26,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "53e84c32-bf83-46bd-add4-035e616b99db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56f52c63-7b3f-404a-a8d0-fb0fb3c8eb43",
            "compositeImage": {
                "id": "3c9bf625-181d-48f2-bd8b-c8f51a6abc1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53e84c32-bf83-46bd-add4-035e616b99db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e12ec98b-b57d-41bb-ae10-cb136a9ab07d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53e84c32-bf83-46bd-add4-035e616b99db",
                    "LayerId": "1fc2c0c8-6bfa-4a58-a6d5-6152c532ff12"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1fc2c0c8-6bfa-4a58-a6d5-6152c532ff12",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "56f52c63-7b3f-404a-a8d0-fb0fb3c8eb43",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}