{
    "id": "30b485d0-2bb8-47ff-a457-7b79134ecd16",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPoof",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "77327e47-11bb-42e2-a1b7-62eec775cf9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30b485d0-2bb8-47ff-a457-7b79134ecd16",
            "compositeImage": {
                "id": "42af9e73-d6af-4eca-97bc-7b10482cd0f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77327e47-11bb-42e2-a1b7-62eec775cf9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "473e1bd6-6889-4c9d-bf06-1a50cbf76861",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77327e47-11bb-42e2-a1b7-62eec775cf9e",
                    "LayerId": "954aa76f-f433-4171-940a-3ae7c8027a65"
                }
            ]
        },
        {
            "id": "af06c01f-d2dd-4280-ba93-e5fe10f60a25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30b485d0-2bb8-47ff-a457-7b79134ecd16",
            "compositeImage": {
                "id": "4f61fff8-b70a-4187-9bc0-c549ed684468",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af06c01f-d2dd-4280-ba93-e5fe10f60a25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "728a351f-70ab-4ca8-9823-7a4842c5b191",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af06c01f-d2dd-4280-ba93-e5fe10f60a25",
                    "LayerId": "954aa76f-f433-4171-940a-3ae7c8027a65"
                }
            ]
        },
        {
            "id": "51443041-caa2-49d7-a28e-1b4781f34245",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30b485d0-2bb8-47ff-a457-7b79134ecd16",
            "compositeImage": {
                "id": "213ff47f-ce15-4c92-ab72-cfb4d687765a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51443041-caa2-49d7-a28e-1b4781f34245",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc72eda7-8045-4f98-bfba-784a17e828cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51443041-caa2-49d7-a28e-1b4781f34245",
                    "LayerId": "954aa76f-f433-4171-940a-3ae7c8027a65"
                }
            ]
        },
        {
            "id": "b96c2e5e-c8db-4e4f-86c2-51cbffb02684",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30b485d0-2bb8-47ff-a457-7b79134ecd16",
            "compositeImage": {
                "id": "b9710e17-eab0-41b3-9cd0-8e15e2de1e48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b96c2e5e-c8db-4e4f-86c2-51cbffb02684",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fa2e9e3-1e97-40d5-8d41-7926febd7239",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b96c2e5e-c8db-4e4f-86c2-51cbffb02684",
                    "LayerId": "954aa76f-f433-4171-940a-3ae7c8027a65"
                }
            ]
        },
        {
            "id": "0c68e6a4-2181-4059-9324-5684021c302c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30b485d0-2bb8-47ff-a457-7b79134ecd16",
            "compositeImage": {
                "id": "34a4eb35-ecee-4415-a5e0-d3148afa6395",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c68e6a4-2181-4059-9324-5684021c302c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dfcd10c-c38a-43c9-95ee-de5f6a645088",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c68e6a4-2181-4059-9324-5684021c302c",
                    "LayerId": "954aa76f-f433-4171-940a-3ae7c8027a65"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "954aa76f-f433-4171-940a-3ae7c8027a65",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "30b485d0-2bb8-47ff-a457-7b79134ecd16",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}