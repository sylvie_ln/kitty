{
    "id": "77932cf0-7d22-49f9-a49d-10ff1b4f29c8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sConfigHat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "87eba51b-2494-4d86-adbf-3e3c386ae4ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77932cf0-7d22-49f9-a49d-10ff1b4f29c8",
            "compositeImage": {
                "id": "bf912578-e9dc-496c-a155-c4ff5fa6c260",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87eba51b-2494-4d86-adbf-3e3c386ae4ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d57398e6-4412-4eab-b743-ea4b10454c2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87eba51b-2494-4d86-adbf-3e3c386ae4ad",
                    "LayerId": "20563626-d1b3-43cc-a1ca-c6e5c5731ce1"
                }
            ]
        },
        {
            "id": "a5493386-de68-4468-a06d-b62fdf9a6d36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77932cf0-7d22-49f9-a49d-10ff1b4f29c8",
            "compositeImage": {
                "id": "855ab7d8-018a-4048-b1be-42bd2d737343",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5493386-de68-4468-a06d-b62fdf9a6d36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d612e185-25c0-42e8-9e5d-66388f85cfb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5493386-de68-4468-a06d-b62fdf9a6d36",
                    "LayerId": "20563626-d1b3-43cc-a1ca-c6e5c5731ce1"
                }
            ]
        },
        {
            "id": "bccd154c-5245-448b-acdd-a734237da9fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77932cf0-7d22-49f9-a49d-10ff1b4f29c8",
            "compositeImage": {
                "id": "e301cbf3-3561-48bf-a5bf-eb1cf9ad8fb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bccd154c-5245-448b-acdd-a734237da9fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1c64562-8a40-4708-a360-0d6ca948444e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bccd154c-5245-448b-acdd-a734237da9fe",
                    "LayerId": "20563626-d1b3-43cc-a1ca-c6e5c5731ce1"
                }
            ]
        },
        {
            "id": "bc8ac824-31f0-4ab0-9b0e-4e42b8805e0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77932cf0-7d22-49f9-a49d-10ff1b4f29c8",
            "compositeImage": {
                "id": "9e4c75dd-548a-4db1-a052-a6b746f37237",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc8ac824-31f0-4ab0-9b0e-4e42b8805e0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e3c1bf2-b4b2-4a8c-97b5-088ae98b2901",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc8ac824-31f0-4ab0-9b0e-4e42b8805e0c",
                    "LayerId": "20563626-d1b3-43cc-a1ca-c6e5c5731ce1"
                }
            ]
        },
        {
            "id": "561a651e-9d04-4ff9-97b0-7a3001b94baf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77932cf0-7d22-49f9-a49d-10ff1b4f29c8",
            "compositeImage": {
                "id": "0cff9314-ebb9-4c11-aa7b-7a3e71c7979b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "561a651e-9d04-4ff9-97b0-7a3001b94baf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eabea8ae-885b-43bf-85c6-58a9fe745aaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "561a651e-9d04-4ff9-97b0-7a3001b94baf",
                    "LayerId": "20563626-d1b3-43cc-a1ca-c6e5c5731ce1"
                }
            ]
        },
        {
            "id": "c85a6cba-ab22-4ce2-a5e2-9cf476bb1b21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77932cf0-7d22-49f9-a49d-10ff1b4f29c8",
            "compositeImage": {
                "id": "23eead70-5cff-4b7c-83b5-3c04bf60ccb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c85a6cba-ab22-4ce2-a5e2-9cf476bb1b21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ffa00d5-619b-4c51-9f2b-4ed4e42fa31a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c85a6cba-ab22-4ce2-a5e2-9cf476bb1b21",
                    "LayerId": "20563626-d1b3-43cc-a1ca-c6e5c5731ce1"
                }
            ]
        },
        {
            "id": "8898651e-5bf1-483e-8402-2eac9390231c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77932cf0-7d22-49f9-a49d-10ff1b4f29c8",
            "compositeImage": {
                "id": "b267d7fa-c11b-42d1-ada6-c0413cb9d62e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8898651e-5bf1-483e-8402-2eac9390231c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a9b2a01-a0d8-4879-9f59-90dac2e564fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8898651e-5bf1-483e-8402-2eac9390231c",
                    "LayerId": "20563626-d1b3-43cc-a1ca-c6e5c5731ce1"
                }
            ]
        },
        {
            "id": "a4cea9c6-9a15-46db-b38a-125fc298374f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77932cf0-7d22-49f9-a49d-10ff1b4f29c8",
            "compositeImage": {
                "id": "550ca739-e44e-4325-b273-241b983cfa1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4cea9c6-9a15-46db-b38a-125fc298374f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25c1b35e-a395-4c58-abf6-0ebec4d4bba8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4cea9c6-9a15-46db-b38a-125fc298374f",
                    "LayerId": "20563626-d1b3-43cc-a1ca-c6e5c5731ce1"
                }
            ]
        },
        {
            "id": "87eb994d-1b5d-4f80-82d2-feeb9c2514b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77932cf0-7d22-49f9-a49d-10ff1b4f29c8",
            "compositeImage": {
                "id": "55a9db80-3b07-42e1-985d-4fd93796cf95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87eb994d-1b5d-4f80-82d2-feeb9c2514b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d82c19a-c915-4721-ace0-d7b94e011661",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87eb994d-1b5d-4f80-82d2-feeb9c2514b7",
                    "LayerId": "20563626-d1b3-43cc-a1ca-c6e5c5731ce1"
                }
            ]
        },
        {
            "id": "1accb6d2-c42a-4e86-8aaa-578af7612b9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77932cf0-7d22-49f9-a49d-10ff1b4f29c8",
            "compositeImage": {
                "id": "0554e036-6184-4f42-9049-66191bbb94df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1accb6d2-c42a-4e86-8aaa-578af7612b9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a7aedb5-fd5f-48a1-afda-95df659326a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1accb6d2-c42a-4e86-8aaa-578af7612b9a",
                    "LayerId": "20563626-d1b3-43cc-a1ca-c6e5c5731ce1"
                }
            ]
        },
        {
            "id": "19109dd6-d120-4c05-8244-95e1c5b0b0f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77932cf0-7d22-49f9-a49d-10ff1b4f29c8",
            "compositeImage": {
                "id": "22398728-5939-43f7-9d63-c7b516be125b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19109dd6-d120-4c05-8244-95e1c5b0b0f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fef8bf46-68c5-42c2-bf8f-8758285990b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19109dd6-d120-4c05-8244-95e1c5b0b0f7",
                    "LayerId": "20563626-d1b3-43cc-a1ca-c6e5c5731ce1"
                }
            ]
        },
        {
            "id": "10e1db86-dd85-446e-a97d-6776a1fc429d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77932cf0-7d22-49f9-a49d-10ff1b4f29c8",
            "compositeImage": {
                "id": "cd5dc42f-c653-4e2f-b1d1-3105262f4052",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10e1db86-dd85-446e-a97d-6776a1fc429d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff3fd658-01bc-4562-98b0-742500a577f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10e1db86-dd85-446e-a97d-6776a1fc429d",
                    "LayerId": "20563626-d1b3-43cc-a1ca-c6e5c5731ce1"
                }
            ]
        },
        {
            "id": "fbefb2fc-3133-4ddf-8089-2a40c45aa1eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77932cf0-7d22-49f9-a49d-10ff1b4f29c8",
            "compositeImage": {
                "id": "0f6d0051-ed5e-43a6-9b8e-85d9d8344f8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbefb2fc-3133-4ddf-8089-2a40c45aa1eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c08cb656-d4ac-4391-9192-8ffcfb695124",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbefb2fc-3133-4ddf-8089-2a40c45aa1eb",
                    "LayerId": "20563626-d1b3-43cc-a1ca-c6e5c5731ce1"
                }
            ]
        },
        {
            "id": "71ad0269-acea-4ba5-a0a6-5c645546fdbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77932cf0-7d22-49f9-a49d-10ff1b4f29c8",
            "compositeImage": {
                "id": "ba3585c9-8376-4bc7-8569-763317c4a1e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71ad0269-acea-4ba5-a0a6-5c645546fdbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "133a1c05-7cdb-4e88-b528-32d875de7427",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71ad0269-acea-4ba5-a0a6-5c645546fdbd",
                    "LayerId": "20563626-d1b3-43cc-a1ca-c6e5c5731ce1"
                }
            ]
        },
        {
            "id": "48b416c1-52b7-4dce-89d2-8f41b2747fa6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77932cf0-7d22-49f9-a49d-10ff1b4f29c8",
            "compositeImage": {
                "id": "f9c95fbc-3412-40b9-84fe-4c3566a5f91b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48b416c1-52b7-4dce-89d2-8f41b2747fa6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bed2e21-a422-45bb-8561-8c3efecf88e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48b416c1-52b7-4dce-89d2-8f41b2747fa6",
                    "LayerId": "20563626-d1b3-43cc-a1ca-c6e5c5731ce1"
                }
            ]
        },
        {
            "id": "13088b65-5c88-480c-aba9-b133400938f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77932cf0-7d22-49f9-a49d-10ff1b4f29c8",
            "compositeImage": {
                "id": "b6151a61-8266-4647-8e19-c03533826f24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13088b65-5c88-480c-aba9-b133400938f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "871918c7-5ac8-4f3f-948a-1136b4935c59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13088b65-5c88-480c-aba9-b133400938f4",
                    "LayerId": "20563626-d1b3-43cc-a1ca-c6e5c5731ce1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "20563626-d1b3-43cc-a1ca-c6e5c5731ce1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "77932cf0-7d22-49f9-a49d-10ff1b4f29c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}