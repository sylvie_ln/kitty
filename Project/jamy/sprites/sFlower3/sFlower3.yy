{
    "id": "757505b3-22d4-498e-a03b-11e9d2cfa696",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFlower3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 5,
    "bbox_right": 28,
    "bbox_top": 18,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6b9e5ac6-6ec1-4ff7-b829-44e6c47f4602",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "757505b3-22d4-498e-a03b-11e9d2cfa696",
            "compositeImage": {
                "id": "59d44e6e-88f0-4074-9557-82f97f182a6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b9e5ac6-6ec1-4ff7-b829-44e6c47f4602",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a58a680-e445-40e8-9e18-57ea061fced3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b9e5ac6-6ec1-4ff7-b829-44e6c47f4602",
                    "LayerId": "4a4b2a88-e768-4f27-9736-eaa3f66fdba5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4a4b2a88-e768-4f27-9736-eaa3f66fdba5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "757505b3-22d4-498e-a03b-11e9d2cfa696",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}