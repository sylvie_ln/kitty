{
    "id": "29d2c696-8fef-41aa-a8b9-9291ed889d18",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHeart",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 10,
    "bbox_right": 21,
    "bbox_top": 10,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a6045cb4-a2b4-4c58-972f-c1885fad2e6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29d2c696-8fef-41aa-a8b9-9291ed889d18",
            "compositeImage": {
                "id": "b93772e9-5a53-4a42-b610-75eea8cea730",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6045cb4-a2b4-4c58-972f-c1885fad2e6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad7d3ee4-91b3-4bbf-8eb5-180a262b601a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6045cb4-a2b4-4c58-972f-c1885fad2e6c",
                    "LayerId": "b1fe3f28-4836-4218-bc89-54be80e5b85f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b1fe3f28-4836-4218-bc89-54be80e5b85f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "29d2c696-8fef-41aa-a8b9-9291ed889d18",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}