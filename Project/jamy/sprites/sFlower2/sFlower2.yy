{
    "id": "589a1553-d9a9-4453-97e5-8826fb9dc57b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFlower2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 5,
    "bbox_right": 28,
    "bbox_top": 15,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4c570e14-3035-4f2d-9e88-843c4773d941",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "589a1553-d9a9-4453-97e5-8826fb9dc57b",
            "compositeImage": {
                "id": "3ae6415b-9b87-4739-89c9-cd83d219fa2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c570e14-3035-4f2d-9e88-843c4773d941",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ea081b2-e555-47b1-9de3-09f6d8c2a3e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c570e14-3035-4f2d-9e88-843c4773d941",
                    "LayerId": "5b4cd01f-f24a-46a5-929e-822b3fe9f9d6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5b4cd01f-f24a-46a5-929e-822b3fe9f9d6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "589a1553-d9a9-4453-97e5-8826fb9dc57b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}