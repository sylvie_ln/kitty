{
    "id": "44e8cb45-bb76-4353-8d94-3910507b4b6e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBackground2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 179,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d71912d3-30d1-4c1f-9683-38a7c6e2e39f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44e8cb45-bb76-4353-8d94-3910507b4b6e",
            "compositeImage": {
                "id": "68b677ef-308b-4513-8991-e4b2be862975",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d71912d3-30d1-4c1f-9683-38a7c6e2e39f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc1d5814-a7d7-4efe-80d1-634ed1a121c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d71912d3-30d1-4c1f-9683-38a7c6e2e39f",
                    "LayerId": "8e47b60b-b021-471f-9c5e-7f60c386761f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "8e47b60b-b021-471f-9c5e-7f60c386761f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "44e8cb45-bb76-4353-8d94-3910507b4b6e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 160,
    "yorig": 90
}