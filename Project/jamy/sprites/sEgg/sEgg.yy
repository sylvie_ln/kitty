{
    "id": "c0dd7289-4ca3-4c9a-b553-5d1aed498175",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEgg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 3,
    "bbox_right": 28,
    "bbox_top": 5,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d1747898-e608-485b-a54e-504c9c233c96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0dd7289-4ca3-4c9a-b553-5d1aed498175",
            "compositeImage": {
                "id": "3b3b7180-9d12-40c0-9271-f34180fd2ff0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1747898-e608-485b-a54e-504c9c233c96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4015654-a3f4-4337-b492-44747bdfc2bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1747898-e608-485b-a54e-504c9c233c96",
                    "LayerId": "46745833-7085-49f9-9df8-21ad795edb70"
                }
            ]
        },
        {
            "id": "1ed86979-f9fc-4153-907a-06c963071738",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0dd7289-4ca3-4c9a-b553-5d1aed498175",
            "compositeImage": {
                "id": "bb51bcdf-ef5f-4f6b-b627-073353924f6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ed86979-f9fc-4153-907a-06c963071738",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da89e63e-c7fa-4d3f-aae7-36e09706cf3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ed86979-f9fc-4153-907a-06c963071738",
                    "LayerId": "46745833-7085-49f9-9df8-21ad795edb70"
                }
            ]
        },
        {
            "id": "d751d666-27bd-4a6e-8e9f-6cf5dc356d4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0dd7289-4ca3-4c9a-b553-5d1aed498175",
            "compositeImage": {
                "id": "7b85e02c-cbc5-48fb-a321-b4c013d242d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d751d666-27bd-4a6e-8e9f-6cf5dc356d4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe73ac0e-049e-45a0-a0ca-440bf6101ed0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d751d666-27bd-4a6e-8e9f-6cf5dc356d4b",
                    "LayerId": "46745833-7085-49f9-9df8-21ad795edb70"
                }
            ]
        },
        {
            "id": "388a0d37-528b-41dc-983b-d2dc31873662",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0dd7289-4ca3-4c9a-b553-5d1aed498175",
            "compositeImage": {
                "id": "be14d1bd-5f0e-4b1a-9e0c-bdcdb84ad66c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "388a0d37-528b-41dc-983b-d2dc31873662",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "129bc410-4b68-45bc-b4d2-ac11e10d8d68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "388a0d37-528b-41dc-983b-d2dc31873662",
                    "LayerId": "46745833-7085-49f9-9df8-21ad795edb70"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "46745833-7085-49f9-9df8-21ad795edb70",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c0dd7289-4ca3-4c9a-b553-5d1aed498175",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}