{
    "id": "4dcc327a-a874-47d4-9e74-98e418b15d89",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBackground5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 179,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "775cf726-d902-4389-8dd5-f2a3c9788cc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dcc327a-a874-47d4-9e74-98e418b15d89",
            "compositeImage": {
                "id": "6ed43474-87e9-42ef-8930-4336f5f3e3e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "775cf726-d902-4389-8dd5-f2a3c9788cc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9492599c-b6fb-496c-9136-7f2995069f43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "775cf726-d902-4389-8dd5-f2a3c9788cc6",
                    "LayerId": "119d63c3-1b84-47d2-b34d-e358e6d749c6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "119d63c3-1b84-47d2-b34d-e358e6d749c6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4dcc327a-a874-47d4-9e74-98e418b15d89",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 160,
    "yorig": 90
}