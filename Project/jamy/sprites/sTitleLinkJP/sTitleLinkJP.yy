{
    "id": "c770c2fe-ccb2-45f7-8951-3a02f1cefb70",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTitleLinkJP",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 179,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5fab7cd6-01f2-4436-8186-070a587aab80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c770c2fe-ccb2-45f7-8951-3a02f1cefb70",
            "compositeImage": {
                "id": "b4276439-b145-4c02-8985-0d7224fe5260",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fab7cd6-01f2-4436-8186-070a587aab80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73bd6f75-9160-4e4d-b4e1-990618c0de09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fab7cd6-01f2-4436-8186-070a587aab80",
                    "LayerId": "b0724344-856e-4b13-9131-ad4e57669959"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "b0724344-856e-4b13-9131-ad4e57669959",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c770c2fe-ccb2-45f7-8951-3a02f1cefb70",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 160,
    "yorig": 90
}