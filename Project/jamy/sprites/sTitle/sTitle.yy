{
    "id": "d75916c3-bd14-47a0-b0b6-e719c3a1e15f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTitle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 179,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7c2aa068-9176-478f-97a8-cf09d78ac14a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d75916c3-bd14-47a0-b0b6-e719c3a1e15f",
            "compositeImage": {
                "id": "a93e56ca-6a5b-4a1c-a687-f320805a1cf4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c2aa068-9176-478f-97a8-cf09d78ac14a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7056d36f-6ff6-4973-9f0b-fd3e608522c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c2aa068-9176-478f-97a8-cf09d78ac14a",
                    "LayerId": "7529d65d-d434-4b08-acf5-3aa30faa57bd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "7529d65d-d434-4b08-acf5-3aa30faa57bd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d75916c3-bd14-47a0-b0b6-e719c3a1e15f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 160,
    "yorig": 90
}