{
    "id": "cd66c184-935c-41cd-8362-69a37e78a241",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBackground4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 179,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5cf102d0-5e0d-4fce-81a2-f9ae754eb38f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd66c184-935c-41cd-8362-69a37e78a241",
            "compositeImage": {
                "id": "939b6c4b-bc6d-4470-bb21-387de9d4013d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cf102d0-5e0d-4fce-81a2-f9ae754eb38f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d2ef0c1-ee79-46dd-bc9d-6120c7bdc1b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cf102d0-5e0d-4fce-81a2-f9ae754eb38f",
                    "LayerId": "d203879c-5a08-432e-98aa-e8c418d3fd23"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "d203879c-5a08-432e-98aa-e8c418d3fd23",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd66c184-935c-41cd-8362-69a37e78a241",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 160,
    "yorig": 90
}