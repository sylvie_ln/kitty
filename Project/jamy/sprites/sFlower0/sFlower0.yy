{
    "id": "62cfaae3-e0c7-40f7-a634-6d194916984c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFlower0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 4,
    "bbox_right": 28,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "01f87f89-8d25-4227-96f7-7c9758f29ecc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62cfaae3-e0c7-40f7-a634-6d194916984c",
            "compositeImage": {
                "id": "984a7315-5a34-476c-b214-9b240e424a38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01f87f89-8d25-4227-96f7-7c9758f29ecc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9972927f-369a-47db-9880-81c21a60bcbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01f87f89-8d25-4227-96f7-7c9758f29ecc",
                    "LayerId": "a7a480f0-e011-47fd-978e-65b94b406e27"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a7a480f0-e011-47fd-978e-65b94b406e27",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "62cfaae3-e0c7-40f7-a634-6d194916984c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}