{
    "id": "337f9a8b-11b8-4976-8f3a-3f3b02d490ad",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTitleLink",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 179,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e17047a0-23ab-4603-b276-658f59cd22e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "337f9a8b-11b8-4976-8f3a-3f3b02d490ad",
            "compositeImage": {
                "id": "3a542f2e-6529-430f-a32b-2c493cee42a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e17047a0-23ab-4603-b276-658f59cd22e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "187ccebe-abb6-4ca7-be48-6390ad5abbb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e17047a0-23ab-4603-b276-658f59cd22e4",
                    "LayerId": "6c428886-e0f0-4888-af5b-cacf818775e4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "6c428886-e0f0-4888-af5b-cacf818775e4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "337f9a8b-11b8-4976-8f3a-3f3b02d490ad",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 160,
    "yorig": 90
}