{
    "id": "135f4f25-454d-48d2-a070-64919d4dcc18",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPoof",
    "eventList": [
        {
            "id": "4d73c632-dfd9-4828-8c66-614bc062864f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "135f4f25-454d-48d2-a070-64919d4dcc18"
        },
        {
            "id": "3f457fc3-08e5-4bfb-8ec8-cb13fdd10489",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "135f4f25-454d-48d2-a070-64919d4dcc18"
        },
        {
            "id": "73f77e67-a0fc-4557-b49b-571b6feaf7a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "135f4f25-454d-48d2-a070-64919d4dcc18"
        }
    ],
    "maskSpriteId": "47656266-f128-4417-adc1-7d34c17390f0",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "30b485d0-2bb8-47ff-a457-7b79134ecd16",
    "visible": true
}