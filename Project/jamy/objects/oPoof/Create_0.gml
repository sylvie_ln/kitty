item = noone;
type = poof_types.none;

enum poof_types {
	create,
	destroy,
	none
}
image_index_previous = image_index;

depth = global.depth_above;

done = false;

image_speed = (game_get_speed(gamespeed_fps)/global.base_room_speed)*(game_get_speed(gamespeed_fps)/fps);