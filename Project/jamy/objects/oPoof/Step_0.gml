if image_index >= 1 and image_index_previous < 1 {
	if type == poof_types.create and place_free(x,y) {
		instance_create_depth(x,y,depth+1,item);
	}
	if type == poof_types.destroy {
		with item {
			if object_index != oHeart {
				instance_destroy();
			}
		}	
	}
	done = true;
}
image_index_previous = image_index;