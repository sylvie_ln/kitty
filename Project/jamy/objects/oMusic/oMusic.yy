{
    "id": "93a52d46-4c7f-45cf-a97c-1003e12b69a9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMusic",
    "eventList": [
        {
            "id": "ee608575-2e82-4337-b56e-5d5f326c7afb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "93a52d46-4c7f-45cf-a97c-1003e12b69a9"
        },
        {
            "id": "4310dcb5-f3b5-430b-8582-9effb2358830",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "93a52d46-4c7f-45cf-a97c-1003e12b69a9"
        },
        {
            "id": "a4436615-d37e-4a41-97f8-26c94eb825ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "93a52d46-4c7f-45cf-a97c-1003e12b69a9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}