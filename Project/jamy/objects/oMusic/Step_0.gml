if keyboard_check(vk_control) and keyboard_check(vk_shift) {
	if keyboard_check_pressed(ord("F")) {
		game_set_speed(game_get_speed(gamespeed_fps)+15,gamespeed_fps);
		//show_debug_message(string(fps_real)+" "+" "+string(fps)+" "+string(game_get_speed(gamespeed_fps)));
		audio_sound_pitch(bgmKitty,audio_sound_get_pitch(bgmKitty)+(1/5));	
	}
	if keyboard_check_pressed(ord("R")) {
		game_set_speed(global.base_room_speed,gamespeed_fps);
		audio_sound_pitch(bgmKitty,1);	
	}
	if keyboard_check_pressed(ord("T")) {
		room_goto(rmTitle);
	}
}

