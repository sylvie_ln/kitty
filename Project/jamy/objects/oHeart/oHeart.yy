{
    "id": "e22598f9-771f-4181-bd19-e229f355e8cc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oHeart",
    "eventList": [
        {
            "id": "722fc486-11bb-4ebb-a523-4389f9cac0e5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "e22598f9-771f-4181-bd19-e229f355e8cc"
        }
    ],
    "maskSpriteId": "47656266-f128-4417-adc1-7d34c17390f0",
    "overriddenProperties": null,
    "parentObjectId": "52c39f00-b80b-4d6f-be2b-972d08d84896",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "29d2c696-8fef-41aa-a8b9-9291ed889d18",
    "visible": true
}