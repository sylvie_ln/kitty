{
    "id": "b81ae5e1-9f3e-4685-b7fe-ba17af58085e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMeat",
    "eventList": [
        {
            "id": "30312181-bd35-457f-bd37-d5ce8b30f790",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "b81ae5e1-9f3e-4685-b7fe-ba17af58085e"
        }
    ],
    "maskSpriteId": "47656266-f128-4417-adc1-7d34c17390f0",
    "overriddenProperties": null,
    "parentObjectId": "52c39f00-b80b-4d6f-be2b-972d08d84896",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "54969dd3-91df-42dd-b722-903fe1959341",
    "visible": true
}