{
    "id": "868a87e1-ef0c-47d2-a3a6-85a3e0e989df",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMountain",
    "eventList": [
        {
            "id": "3a7113c1-afc2-4431-8eb6-2f6ba7176b2b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "868a87e1-ef0c-47d2-a3a6-85a3e0e989df"
        }
    ],
    "maskSpriteId": "47656266-f128-4417-adc1-7d34c17390f0",
    "overriddenProperties": null,
    "parentObjectId": "5aa6c979-6a99-47db-bc28-20b83a914851",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b185d082-fa27-48ed-bcbb-9f2999c00db2",
    "visible": true
}