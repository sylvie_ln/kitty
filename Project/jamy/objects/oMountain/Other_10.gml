with oKittey {
	var xp = x;
	var yp = y;
	var g = 4;
	x = floor(irandom_range(0,room_width)/g)*g;
	y = floor(irandom_range(0,room_height)/g)*g;
	var count = 16;
	var collision = script_execute(check_collision,x,y);
	while collision[0] {
		x = floor(irandom_range(0,room_width)/g)*g;
		y = floor(irandom_range(0,room_height)/g)*g;
		count--;
		if count == 0 { 
			x = xp;
			y = yp;
			break; 
		}
		collision = script_execute(check_collision,x,y);
	}
	instance_create_depth(x,y,depth,oPoof);
}