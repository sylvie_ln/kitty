size = 512;

if !surface_exists(stencil) {
	stencil = surface_create(size,size);
}
if !surface_exists(surfy) {
	surfy = surface_create(size,size);
}

if surface_exists(stencil) {
	surface_set_target(stencil);
	draw_clear_alpha(c_black,0);
	with oBlock {		
		draw_set_color(c_black);
		if os_browser != browser_not_a_browser {
			draw_rectangle(x-1-16,y-1-16,x+33-16,y+33-16,false);
		} else {
			draw_rectangle(x-1-16,y-1-16,x+32-16,y+32-16,false);	
		}
	}
	with oBlock {
		draw_sprite_ext(sprite_index,0,x,y,image_xscale,image_yscale,image_angle,c_white,1);
	}
	surface_reset_target();
}
if surface_exists(surfy) {
	surface_set_target(surfy);
	draw_clear_alpha(c_black,0);
	draw_sprite_tiled_ext(bg,0,0+x_offset,0+y_offset,bg_scale,bg_scale,image_blend,image_alpha);
	gpu_set_blendmode_ext(bm_dest_color,bm_zero);
	draw_surface(stencil,0,0);
	gpu_set_blendmode(bm_normal);
	surface_reset_target();
	draw_surface(surfy,0,0);
}

if game_get_speed(gamespeed_fps) > global.base_room_speed {
	y_speed = global.ys*(game_get_speed(gamespeed_fps)/fps);
} else {
	y_speed = global.ys;
}
x_offset += x_speed;
y_offset += y_speed;

draw_set_color(c_black);
if global.japanese {
	draw_sprite(sKittyFooterJP,0,0,160);
} else {
	draw_sprite(sKittyFooter,0,0,160);
}
