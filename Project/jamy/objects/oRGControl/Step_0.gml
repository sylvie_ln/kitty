if input_pressed_repeat("Up") {
	global.ys -= 0.05;
}
if input_pressed_repeat("Down") {
	global.ys += 0.05;
}
if keyboard_check(vk_control) and keyboard_check(vk_shift) {
	if keyboard_check_pressed(ord("J")) {
		global.japanese = !global.japanese;
	}
}