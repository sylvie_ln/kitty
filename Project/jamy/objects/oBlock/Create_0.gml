if image_xscale != 1 or image_yscale != 1 {
	for(var i=0; i<image_xscale; i++) {
		for(var j=0; j<image_yscale; j++) {
			instance_create_depth(x+i*32,y+j*32,depth,object_index);
		}	
	}
	instance_destroy(id,false);
	exit;
}
center_origin(sprite_index);