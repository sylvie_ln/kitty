{
    "id": "89ad6863-1e89-4260-a199-ec787c178342",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBlock",
    "eventList": [
        {
            "id": "36ac8869-7588-4a71-b14a-7c67cb23104f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "89ad6863-1e89-4260-a199-ec787c178342"
        },
        {
            "id": "dca20eaf-33e2-4d99-ad15-9ed397606d97",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "89ad6863-1e89-4260-a199-ec787c178342"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "ccbcd56d-e4b9-4235-87f4-c7e1c23ba859",
    "visible": false
}