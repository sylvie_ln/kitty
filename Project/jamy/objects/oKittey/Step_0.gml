audio_sound_pitch(sfxMeow,random_range(0.7,1.3));

while process_messages() {
	switch(message_type) {
		case message_types.level_win:
			dance = true;
			audio_play_sound(sfxMeow,100,false);
		break;
	}
}

if dance {
	var step = dance_steps[|dance_pose];
	image_xscale = step[0];
	image_index = step[1]+2*step[2];
	image_angle = step[3];
	dance_timer++;
	if dance_timer >= dance_time {
		dance_pose++;
		if dance_pose >= ds_list_size(dance_steps) {
			if room_next(room) != rmAdvertisement {
				room_goto_next();	
			} else {
				game_set_speed(game_get_speed(gamespeed_fps)+15,gamespeed_fps);
				audio_sound_pitch(bgmKitty,audio_sound_get_pitch(bgmKitty)+(1/5));
				room_goto(rmTitle);	
			}
		}
		dance_timer = 0;
	}
	exit;
} 

hdir_previous = hdir;
vv_previous = vv;

var left = input_held("Left");
var right = input_held("Right");
hdir = right-left;
if left and right {
	hdir = (input_held_time("Left") <= input_held_time("Right")) ? -1 : 1;	
}

hv = spd*hdir;

var jump_pressed = input_pressed("Jump");
var jump_released = input_released("Jump");
var ong = on_ground();

if !ong {
	vv += grav;	
}

if ong and jump_pressed {
	audio_play_sound(sfxMeow,100,false);
	vv = -jump;	
}

if jump_released and vv < 0 {
	vv /= 2;	
}

if !move(hv,0) { hv = 0; }
if !move(vv,1) { vv = 0; }

mask_index = sItemMask;
var xp = x + image_xscale*32;
var yp = y;
var temp_list = ds_list_create();
var num = instance_place_list(xp,yp,oItem,temp_list,true);
var item = noone;
if num != 0 {
	item = temp_list[|0];
}
ds_list_destroy(temp_list);
if input_pressed("Magic") and !place_meeting(xp,yp,oPoof) {
	audio_play_sound(sfxMeow,100,false);
	if item == noone {
		var poof = instance_create_depth(xp,yp,depth,oPoof);	
		poof.type = poof_types.create;
		poof.item = global.stage_item;
	} else {
		var poof = instance_create_depth(item.x,item.y,depth,oPoof);	
		poof.type = poof_types.destroy;
		poof.item = item;
	}
}
mask_index = sprite_index;

if hdir != 0 {
	image_xscale = hdir;
}

if !ong or vv != 0 {
	// tail and legs stay still
	tail = floor(tail);
	legs = 1
} else {
	if hdir == 0 {
		// tail and legs stay still
		tail = floor(tail);
		legs = 0
	} else {
		if hdir_previous == 0 {
			legs = 1-floor(legs);
		} else if (vv == 0 and vv_previous > 0) {
			legs = 1-floor(legs);
		} else {
			legs += 5/global.base_room_speed;
			if legs >= 2 { legs -= 2; }
			tail += 5/global.base_room_speed;
			if tail >= 2 { tail -= 2; }
		}
	}
}
image_index = floor(tail)+2*floor(legs);

if game_get_speed(gamespeed_fps) > global.base_room_speed {
	event_user(0);
} else {
	event_user(1);	
}