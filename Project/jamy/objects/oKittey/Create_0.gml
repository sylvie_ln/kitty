event_inherited();

base_spd = 2;
spd = 2;
jump = sqrt(2*grav*33);

hdir = 0;
hdir_previous = 0;
vv_previous = 0;

tail = 0;
legs = 0;
image_speed = 0;

init_messages();

dance = false;
dance_pose = 0;
dance_steps = 
list_create(
[1,0,0,0],
[1,1,1,0],
[-1,1,0,0],
[-1,0,1,0],
[1,0,0,0],
[1,1,1,90],
[1,1,0,180],
[1,0,1,270],
[1,0,0,0],
[-1,1,0,0],
[1,1,1,0],
[1,1,1,0],
[1,1,1,0],
)

dance_time = global.base_room_speed*(5/13);
dance_timer = 0; 