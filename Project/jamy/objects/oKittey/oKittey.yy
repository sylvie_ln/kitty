{
    "id": "4bfb2562-ce02-47c6-bf9e-64db60586ef6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oKittey",
    "eventList": [
        {
            "id": "039c6e74-e82a-478d-9c31-4816bdca3e9c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4bfb2562-ce02-47c6-bf9e-64db60586ef6"
        },
        {
            "id": "ed6e5393-51df-4523-8a0c-8cfa8778160b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4bfb2562-ce02-47c6-bf9e-64db60586ef6"
        },
        {
            "id": "82b6d456-4ae6-4f89-9bce-f61f39432a7c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "4bfb2562-ce02-47c6-bf9e-64db60586ef6"
        },
        {
            "id": "5798019e-4c2f-439d-9d47-fa0e654b4d39",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "4bfb2562-ce02-47c6-bf9e-64db60586ef6"
        },
        {
            "id": "27a675f8-945d-4fec-9292-e227da5f62c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "4bfb2562-ce02-47c6-bf9e-64db60586ef6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "3c65003b-df41-46ef-acd4-f04ae3a9d5b5",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4ebe0adc-d323-4261-acfb-c94c655801eb",
    "visible": true
}