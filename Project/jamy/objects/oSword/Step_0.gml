if fly {
	if target == noone or is_undefined(target) or !instance_exists(target) {
		target = instance_nearest(x,y,oBlock);
		if target == noone or is_undefined(target) or !instance_exists(target)  {
			exit;	
		}
	}
	var dir = point_direction(x,y,target.x,target.y);
	spd += acc;
	x += lengthdir_x(spd,dir);
	y += lengthdir_y(spd,dir);
	with instance_place(x,y,oBlock) {
		var poof = instance_create_depth(x,y,depth,oPoof);	
		poof.type = poof_types.destroy;
		poof.item = id;
		with other { instance_destroy(); }
	}
}