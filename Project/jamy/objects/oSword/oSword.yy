{
    "id": "fadf1ef3-9298-4b97-b91d-2339a6756c4c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSword",
    "eventList": [
        {
            "id": "1ba5958e-fc57-4838-9f64-5e148bde1b4b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "fadf1ef3-9298-4b97-b91d-2339a6756c4c"
        },
        {
            "id": "f5715b0b-21d0-43de-848c-25f8d463338a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fadf1ef3-9298-4b97-b91d-2339a6756c4c"
        },
        {
            "id": "a35f9d5f-9e74-4288-a571-4af0b8604a3f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fadf1ef3-9298-4b97-b91d-2339a6756c4c"
        }
    ],
    "maskSpriteId": "47656266-f128-4417-adc1-7d34c17390f0",
    "overriddenProperties": null,
    "parentObjectId": "5aa6c979-6a99-47db-bc28-20b83a914851",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "56f52c63-7b3f-404a-a8d0-fb0fb3c8eb43",
    "visible": true
}