if keyboard_check(vk_control) and keyboard_check(vk_shift) {
	if keyboard_check_pressed(ord("J")) {
		global.japanese = !global.japanese;
		if global.japanese {
			layer_background_sprite(layer_background_get_id("Background"),sTitleLinkJP);	
		} else {
			layer_background_sprite(layer_background_get_id("Background"),sTitleLink);	
		}
	}
	if keyboard_check_pressed(ord("G")) {
		room_goto(rmTitle);
	}

}