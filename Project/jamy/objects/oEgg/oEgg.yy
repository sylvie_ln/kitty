{
    "id": "2027591c-2aaf-406e-acfd-e5323ea6d0ed",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oEgg",
    "eventList": [
        {
            "id": "a2ed312c-3f04-4063-b7cc-d95a9a19b82f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "2027591c-2aaf-406e-acfd-e5323ea6d0ed"
        },
        {
            "id": "b92812f0-4f04-4621-8e00-8b09d1088552",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2027591c-2aaf-406e-acfd-e5323ea6d0ed"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "607b9bcd-3ec1-4872-afdc-bf67d6acb522",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c0dd7289-4ca3-4c9a-b553-5d1aed498175",
    "visible": true
}