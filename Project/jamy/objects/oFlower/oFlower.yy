{
    "id": "70d2fcf7-4746-421c-b3ab-955532f2aa2e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFlower",
    "eventList": [
        {
            "id": "5dd80a02-78c4-4448-aeae-63c6942347c2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "70d2fcf7-4746-421c-b3ab-955532f2aa2e"
        },
        {
            "id": "3e1889ed-0c8f-467c-b415-45158e4ee267",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "70d2fcf7-4746-421c-b3ab-955532f2aa2e"
        },
        {
            "id": "1eecc221-fb47-4bec-bb63-21839b37ffcf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "70d2fcf7-4746-421c-b3ab-955532f2aa2e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "607b9bcd-3ec1-4872-afdc-bf67d6acb522",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "62cfaae3-e0c7-40f7-a634-6d194916984c",
    "visible": true
}