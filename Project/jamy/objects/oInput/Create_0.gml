enum input_kind {
	key,
	mouse,
	gamepad_button,
	gamepad_axis,
	gamepad_hat,
	waiting
}

enum input_read_type {
	held,
	pressed,
	released,
	held_time,
	pressed_repeat,
	raw_axis
}

actions = ds_map_create();
action_list = ds_list_create();
gpad_map = ds_map_create();
held_time_map = ds_map_create();
previous_axis_map = ds_map_create();
previous_hat_map = ds_map_create();
event_perform(ev_step,ev_step_end);

multiplayer = false;
gpid = 0;

act1 = "Jump";
act2 = "Magic";

input_define_action("Up");
input_define_action("Down");
input_define_action("Left");
input_define_action("Right");
input_define_action(act1);
input_define_action(act2);
input_define_action("Start");

input_assign_key("Up",vk_up,ord("W"));
input_assign_key("Down",vk_down,ord("S"));
input_assign_key("Left",vk_left,ord("A"));
input_assign_key("Right",vk_right,ord("D"));
input_assign_key(act1,vk_space,vk_enter,ord("X"));
input_assign_key(act2,vk_control,vk_shift,ord("C"));
input_assign_key("Start",vk_enter,vk_space);

input_assign_gamepad_button("Up",gp_padu);
input_assign_gamepad_button("Down",gp_padd);
input_assign_gamepad_button("Left",gp_padl);
input_assign_gamepad_button("Right",gp_padr);
input_assign_gamepad_button(act1,gp_face2,gp_face3);
input_assign_gamepad_button(act2,gp_face1,gp_face4);
input_assign_gamepad_button("Start",gp_start,gp_face1,gp_face2,gp_face3,gp_face4);

input_assign_gamepad_axis("Up",gp_axislv,-1,gp_axisrv,-1);
input_assign_gamepad_axis("Down",gp_axislv,1,gp_axisrv,1);
input_assign_gamepad_axis("Left",gp_axislh,-1,gp_axisrh,-1);
input_assign_gamepad_axis("Right",gp_axislh,1,gp_axisrh,1);

input_assign_gamepad_hat("Up",0,1);
input_assign_gamepad_hat("Down",0,4);
input_assign_gamepad_hat("Left",0,8);
input_assign_gamepad_hat("Right",0,2);

global.key_repeat_speed = room_speed div 6;
global.key_repeat_delay = room_speed div 6;