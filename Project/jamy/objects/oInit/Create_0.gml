global.sylvie_font = font_add_sprite(sSylvieFont,ord(" "),true,1);
global.sylvie_font_bold = font_add_sprite(sSylvieFontB,ord(" "),true,1);
global.sylvie_font_bold_2x = font_add_sprite(sSylvieFontB2x,ord(" "),true,2);

global.game_save_map = ds_map_create();
global.options_save_map = ds_map_create();
global.game_save_file = "save.fluffy"
global.options_save_file = "options.fluffy"

init_outline();

global.view_width = room_width;
global.view_height = room_height;

global.max_scale = compute_max_scale();

global.scale = global.max_scale;
global.volume = 1;

instance_create_depth(0,0,0,oInput);
instance_create_depth(0,0,0,oMusic);

global.scale = min(global.scale,global.max_scale);
window_set_scale(global.scale);
audio_set_master_gain(0,global.volume)

global.stage_item = oFlower;

global.depth_middle = 0;
global.depth_above = -10;
global.depth_below = 10;
global.depth_way_below = 20;

audio_stop_all()
audio_play_sound(bgmKitty,1000,true);

global.base_room_speed = 30;
global.ys = 0.25;

global.japanese = false;

alarm[0] = 1;